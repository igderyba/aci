#!/bin/bash
#
#The script is showing ACI leaf port usage
#
#Made by Igor Derybas

if [ "$1" = "" ] || [ "$2" = "" ] ; then
  echo "Usage:"
  echo "show_port.sh.sh <node ID> <Port number> <(Optional) Pod ID>"
  echo "Example: ./show_port.sh 101 47 1"
  exit 1
fi

if [ "$3" = "" ] ; then
        pod=1
else
        pod=$3
fi

node=$1
port=$2

pc_cmd="moquery -c ethpmPhysIf -f 'ethpm.PhysIf.dn == \"topology/pod-$pod/node-$node/sys/phys-[eth1/$port]/phys\"' | grep bundleIndex | grep bundleIndex | awk '{print \$3}'"
pc=`eval $pc_cmd`

if [ "$pc" != "unspecified" ] ; then
        pg_cmd="moquery -c pcAggrIf -f 'pc.AggrIf.dn==\"topology/pod-$pod/node-$node/sys/aggr-[$pc]\"'| grep name | awk '{print \$3}'"
        pn=`eval $pg_cmd`
        echo "Usage of Port eth1/$port on Node-$node in Pod-$pod:"
        echo "Port is a member of Port-Channel, Interface Policy Group name - $pn"
else
        pn="eth1/$port"
        echo "Usage of Port eth1/$port on Node-$node in Pod-$pod:"
fi

echo "Tenant Policies related:"
moquery -c fvIfConn | grep $pn | grep node-$node| sort | awk '{print $3}' | sed 's/.*uni\//Path: /g' | sed 's/\]\/node-.*\/stpathatt.*\/conn-\[/ Static Encap: /g' |  sed 's/\]\/node-.*\/dyatt.*\/conn-\[/ Dynamic Encap: /g' | sed 's/\]-\[.*//g'
